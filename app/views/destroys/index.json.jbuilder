json.array!(@destroys) do |destroy|
  json.extract! destroy, :id, :post_id, :body
  json.url destroy_url(destroy, format: :json)
end
