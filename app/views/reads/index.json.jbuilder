json.array!(@reads) do |read|
  json.extract! read, :id, :title, :body
  json.url read_url(read, format: :json)
end
