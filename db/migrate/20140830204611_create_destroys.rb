class CreateDestroys < ActiveRecord::Migration
  def change
    create_table :destroys do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
